import config
import mysql.connector
# from mysql.connector import errorcode


db = False


def connect():
    global db
    # TODO: return same connection if alive
    if db:
        return db
    for server in config.servers:
        try:
            db = mysql.connector.connect(host=server,
                                         user=config.user,
                                         passwd=config.password,
                                         database=config.database)
        except mysql.connector.Error as err:
            print "Note: can not connect to", server
            pass
        else:
            return db
    print "Error: can not connect to any server."
    return False


def close_connection():
    global db
    if db:
        db.close()


def increment(counter, value=1):
    counter = str(counter)
    value = int(value)
    if 1 > len(counter) > 256:
        return False

    cnx = connect()
    if not cnx:
        return False

    cursor = cnx.cursor()

    try:
        cursor.execute("UPDATE `counters`.`counters` "
                       "SET `value` = `value` + %s "
                       "WHERE `counters`.`name` = %s ", (value, counter,))
        cursor.close()
        cnx.commit()
    except mysql.connector.Error as err:
        cursor.close()
        print(err.msg)
        return False

    return True


def reset(counter):
    counter = str(counter)
    if 1 > len(counter) > 256:
        return False

    cnx = connect()
    if not cnx:
        return False

    cursor = cnx.cursor()

    try:
        cursor.execute("INSERT INTO `counters`.`counters` (`name`, `value`) "
                       "VALUES (%s, '0') "
                       "ON DUPLICATE KEY UPDATE "
                       "`value` = '0'", (counter,))
        cursor.close()
        cnx.commit()
    except mysql.connector.Error as err:
        print(err.msg)
        cursor.close()
        return False

    return True


def get(counter):
    counter = str(counter)
    if 1 > len(counter) > 256:
        return False

    cnx = connect()
    if not cnx:
        return False

    cursor = cnx.cursor()

    try:
        result = cursor.execute("SELECT `value` "
                                "FROM `counters`.`counters` "
                                "WHERE `counters`.`name` = %s ", (counter,))
        for value in cursor:
            cursor.close()
            return value[0]
    except mysql.connector.Error as err:
        cursor.close()
        print(err.msg)
        return False

    cursor.close()
    return False


def create_table():
    cnx = connect()
    if not cnx:
        return False
    cursor = cnx.cursor()

    try:
        cursor.execute("CREATE TABLE `counters` ("
                       "  `name` varchar(256) NOT NULL,"
                       "  `value` integer NOT NULL,"
                       "  PRIMARY KEY (`name`)"
                       ")"
        )
    except mysql.connector.Error as err:
        cursor.close()
        # if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
        #     print("Table already exists.")
        #     return True
        # else:
        print(err.msg)
        return False

    cursor.close()
    print "Created table."
    return True
